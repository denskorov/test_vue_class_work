// import Vue from 'vue' vue 2
// Vue.use(Vuex) vue 2
import {createStore} from 'vuex' // vue 3
// import Vuex from 'vuex' // vue 2

const store = createStore({
// const store = new Vuex.Store({ // vue 2

    modules:{
        users:{

        },

    },

    state: () => ({
        player_1_name: '',
        player_2_name: '',
        // qq: {
        //     ff: {
        //         kk: ''
        //     }
        // }
    }), //інфо
    mutations: {
        setPlayer1Name(state, name) {
            state.player_1_name = name
        },
        setPlayer2Name(state, name) {
            state.player_2_name = name
        },
    }, // функції які назначення для внесення змін
    getters: {
        players(state){
            return [state.player_1_name, state.player_2_name]
        }
    }, //  = computed
    actions: {
        // eslint-disable-next-line no-unused-vars
        async getUsers(ctx, cb){
            const response = await fetch('https://jsonplaceholder.typicode.com/users')
            cb(await response.json())
        }
    } // = methods

})

export default store
